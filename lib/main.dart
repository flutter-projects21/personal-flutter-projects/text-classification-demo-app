import 'package:flutter/material.dart';

import 'classifier.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late TextEditingController _controller;
  late Classifier _classifier;
  late List<Widget> _children;
  final positiveColor = Colors.green;
  final negativeColor = Colors.redAccent;
  final disabledColor = Colors.grey;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
    _classifier = Classifier();
    _children = [];
    _children.add(Container());
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: const Text('Text classification'),
        ),
        body: Container(
          padding: const EdgeInsets.all(4),
          child: Column(
            children: <Widget>[
              Expanded(
                  child: ListView.builder(
                itemCount: _children.length,
                itemBuilder: (_, index) {
                  return _children[index];
                },
              )),
              Container(
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.green),
                    borderRadius: BorderRadius.circular(20)),
                child: Row(children: <Widget>[
                  Expanded(
                    child: TextField(
                      decoration: const InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Write some text here'),
                      controller: _controller,
                    ),
                  ),
                  TextButton(
                    child: const Text('Classify'),
                    onPressed: () {
                      if (_controller.text.isEmpty) {
                        return;
                      }
                      final text = _controller.text;
                      final prediction = _classifier.classify(text);

                      var mainColor = prediction[1] > prediction[0]
                          ? positiveColor
                          : negativeColor;
                      setState(() {
                        _children.add(Dismissible(
                          key: GlobalKey(),
                          onDismissed: (direction) {},
                          child: Container(
                            padding: const EdgeInsets.all(16),
                            margin: const EdgeInsets.all(8),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(color: mainColor, width: 4),
                                borderRadius: BorderRadius.circular(20)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  text,
                                  style: const TextStyle(fontSize: 16),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Spacer(),
                                    Icon(
                                      Icons.thumb_up,
                                      color: prediction[1] > prediction[0]
                                          ? positiveColor
                                          : disabledColor,
                                      size: 45,
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      "${prediction[1].toStringAsPrecision(2)} %",
                                      style: TextStyle(
                                          color: prediction[1] > prediction[0]
                                              ? positiveColor
                                              : disabledColor,
                                          fontSize: 24),
                                    ),
                                    Spacer(flex: 2),
                                    Text(
                                      "${prediction[0].toStringAsPrecision(2)} %",
                                      style: TextStyle(
                                        color: prediction[0] > prediction[1]
                                            ? negativeColor
                                            : disabledColor,
                                        fontSize: 24,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Icon(
                                      Icons.thumb_down,
                                      color: prediction[0] > prediction[1]
                                          ? negativeColor
                                          : disabledColor,
                                      size: 45,
                                    ),
                                    Spacer(),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ));
                        _controller.clear();
                      });
                    },
                  ),
                ]),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
