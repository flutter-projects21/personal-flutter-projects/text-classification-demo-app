# Text Classification Demo App

A simple flutter demo app for text classification


## UI

![smple 1](readme_images/1.gif)
![smple 2](readme_images/2.gif)
![smple 3](readme_images/3.gif)


## Classification Model

### Name
`
Sentiment Analyzer (AverageWordVecModelSpec)
`

### Description
`
Detect if the input text's sentiment is positive or negative. The model was
trained on the IMDB Movie Reviews dataset so it is more accurate when input text
a movie review.
`

### Vocab
`
10000 words
`
### Version
`
vl
`
### Author
`
TensorFlow
`

### License

`
Apache License. Version 2.0
`
http://www.apache.org/licenses/LlCENSE-2.0.


## Tensors

![inputs](readme_images/inputs.png)

![output](readme_images/outputs.png)



# Contact With Me

## Rasoul Abouassaf - AI Engineer

* [Rasoul Abouassaf- Linked-In](https://www.linkedin.com/in/rasoul-abouassaf-149005172/)

* [rasoul.19.assaf - GitLab](https://www.linkedin.com/in/rasoul-abouassaf-149005172/)

* Email: rasoul.a.assaf@gmail.com
* [My Portfolio](https://www.canva.com/design/DAFNzXHIg5g/d11mR0vBrQUpW-wvBum-kA/edit?utm_content=DAFNzXHIg5g&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton) 
